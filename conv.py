#!/usr/bin/python3
import codecs
import argparse
import re
import sys, os
from datetime import datetime, timezone


def sanitize(value, input_type):
    clean_value = value

    if input_type == "hex":
        if re.match("^[a-f0-9]*h$", value):
            clean_value = value[0:-1]
        elif re.match(r"^0x[a-z0-9]$", value):
            clean_value = value[2::]

    return clean_value


def try_resolve_type(values):
    # Best guess input type based on the argument.
    # Of course this is never going to be fool proof, so these are just my preferred defaults.

    if isinstance(values, str):
        values = [values]

    input_type = None
    potential_types = set()

    for value in values:
        if re.match(r"^0x[a-fA-F0-9]{1,4}$", value) or re.match(
            "^[a-fA-F0-9]{1,4}h$", value
        ):
            potential_types.add("hex")

        elif re.match("^[0-1a-f]{2}$", value):
            potential_types.add("byte")

        elif re.match("^[0-1]{8}$", value):
            potential_types.add("bin")

        elif re.match(
            "^[0-9]{4}-[0-1]{1}-[0-9]{1}-[0-3]{1}[0-9]{1}T[0-2]{1}[0-9]{1}:[0-6]{1}[0-9]:[0-6]{1}[0-9]{1}$",
            value,
        ) or re.match("^[0-9]{4}-[0-1]{1}[0-9]{1}-[0-3]{1}[0-9]{1}", value):
            potential_types.add("iso8601")

        elif re.match("^[0-9]{8,10}$", value):
            potential_types.add("ts")

        elif re.match("^[0-9]+$", value):
            potential_types.add("dec")

        elif re.match("^[0-9a-zA-Z]{1}$", value):
            potential_types.add("str")

        elif re.match("^[\x00-\x7F]+$", value):
            potential_types.add("str")

    input_type = choose_type_from_options(potential_types)

    # Determine default output
    if input_type == "str":
        default_output = "byte"
    if input_type == "hex":
        default_output = "dec"
    elif input_type == "byte":
        default_output = "dec"
    elif input_type == "dec":
        default_output = "hex"
    elif input_type == "bin":
        default_output = "byte"
    elif input_type == "byte":
        default_output = "dec"
    elif input_type == "ts":
        default_output = "iso8601"
    elif input_type == "iso8601":
        default_output = "ts"

    if input_type is None:
        raise TypeError(f"Could not auto-resolve input {values} to a type.")

    return input_type, default_output


def choose_type_from_options(potential_types):
    # now we have to choose one...
    # print(potential_types)
    if "byte" in potential_types:
        return "byte"
    else:
        return potential_types.pop()


def get_result(value, input_type, output_type):

    output = ""

    # Conversions from hex
    if input_type == "hex" and output_type == "dec":
        output = str(int(value, 16))

    # Conversions from str
    elif input_type == "str" and output_type == "byte":
        # tread the input as byte array and convert the strings to
        # their ascii numeric values
        output = " ".join([f"{ord(y):#0{4}x}" for y in value])

    elif input_type == "str" and output_type == "bin":
        values = [f"{(ord(y)):08b}" for y in value]
        output = " ".join(values)

    # Conversions from decimals
    elif input_type == "dec" and output_type == "byte":
        output = f"{int(value):#0{4}x}"

    # Conversions from bytes
    elif input_type == "byte" and output_type == "str":
        bytes = value.split(" ")

        ascii_chars = [chr(int(byte, 16)) for byte in bytes]
        output = "".join(ascii_chars)

    elif input_type == "byte" and output_type == "bin":
        # take ordinal value of each char in input and represent its byte value (ASCII)
        values = [f"{(ord(y)):08b}" for y in value]
        output = " ".join(values)

    # Conversions from binary
    elif input_type == "bin" and output_type == "dec":
        output = str(int(value, 2))

    elif input_type == "bin" and output_type == "byte":
        output = hex(int(value, 2))

    # Conversions from timestamp
    elif input_type == "ts" and output_type == "iso8601":
        # Return a ISO8601 formatted date from a unix timestamp in seconds
        output = (
            f"{datetime.utcfromtimestamp(int(value)).strftime('%Y-%m-%dT%H:%M:%S%z')}"
        )

    elif input_type == "iso8601" and output_type == "ts":
        # Return a utc timestamp from a utc formatted dateTime object
        formats = ["%Y-%m-%dT%H:%M:%S", "%Y-%m-%d"]
        for fmt in formats:
            try:
                output = f"{int(datetime.strptime(value, fmt).replace(tzinfo=timezone.utc).timestamp())}"
            except ValueError:
                continue

    return output


def convert(values, input_type=None, output_type=None):

    if input_type is None or output_type is None:
        if values == "now":
            values = [str(int(datetime.now().timestamp()))]

        guessed_input_type, default_output_type = try_resolve_type(values)
        if input_type is None:
            input_type = guessed_input_type
        if output_type is None:
            output_type = default_output_type

    # Remove common values like 0x and ..h from value
    if isinstance(values, list):
        clean_values = [sanitize(x, input_type) for x in values]
    else:
        clean_values = [sanitize(values, input_type)]

    results = " ".join([get_result(x, input_type, output_type) for x in clean_values])

    if len(results) == 0:
        results = f"Dunno..."

    if isinstance(values, list):
        values = " ".join(values)

    print(f"{values} \t ({input_type})\t => \t{results}\t ({output_type})")


if __name__ == "__main__":
    supported_types = ["0x", "hex", "byte", "bin", "ts", "iso8601"]

    parser = argparse.ArgumentParser(description="Convert stuff")
    parser.add_argument("values", help="The value(s) to convert")
    parser.add_argument(
        "-f", "--from", choices=supported_types, required=False, help="input type",
    )

    parser.add_argument(
        "-t", "--to", choices=supported_types, required=False, help="output format"
    )
    args = vars(parser.parse_args())

    input_type = args.get("from")
    output_type = args.get("to")

    value = args.get("values")
    convert(value, input_type, output_type)

    # convert("0xA0")
    # convert("ABCDEF", output_type="byte")
    # convert("1570438034", "ts")
    # convert("01001001", output_type="byte")
    # convert("61", input_type="byte", output_type="bin")
    # convert("61", input_type="byte", output_type="str")
    # convert("a", input_type="str", output_type="byte")
    # convert("hello world!")
    # convert(["31", "31", "3d", "45"])
    # convert(["0xef", "0x31", "0x3d", "0x45"])
    # convert(["0xef", "0x31", "0x3d", "0x45"], output_type="str")
    # convert(["01100100", "01001001"], output_type="byte")
    # convert(["01100100", "01001001"], output_type="dec")
    # convert("hello", output_type="byte")
    # convert("2020-01-31T18:40:00")
    # convert("1970-01-01T00:00:00")
